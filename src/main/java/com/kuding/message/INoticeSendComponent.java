package com.kuding.message;

import com.kuding.content.ExceptionNotice;

public interface INoticeSendComponent {

	public void send(ExceptionNotice exceptionNotice);
}
